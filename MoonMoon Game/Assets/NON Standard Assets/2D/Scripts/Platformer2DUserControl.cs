using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets._2D;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[RequireComponent(typeof (PlatformerCharacter2D))]
public class Platformer2DUserControl : MonoBehaviour
{
    private PlatformerCharacter2D m_Character;
    private bool m_Jump;
	public static bool interacting = false;

	[SerializeField] Transform pistol = null;
	[SerializeField] Transform assault = null;
	[SerializeField] Transform c4 = null;

	[SerializeField] Rigidbody2D pistolProjectile = null;
	[SerializeField] Rigidbody2D assaultProjectile = null;
	[SerializeField] Rigidbody2D c4Projectile = null;

	int oneAmmo = -1;
	int twoAmmo = 50;
	int threeAmmo = 10;
	Rigidbody2D rigidbody = null;

	[SerializeField] AudioSource gunAudio = null;

	[Header("Pistol")]
	[SerializeField] List<AudioClip> pistolSounds = new List<AudioClip>();
	float pistolShotTime = 0.35f;
	float pistolShotClock = 0;

	[Header("Assualt")]
	[SerializeField] List<AudioClip> assaultSounds = new List<AudioClip>();
	float assaultShotTime = 0.17f;
	float assaultShotClock = 0;

	[Header("C4")]
	[SerializeField] List<AudioClip> c4Sounds = new List<AudioClip>();
	float c4ShotTime = 1.5f;
	float c4ShotClock = 0;

	int health = 100;
	[SerializeField] HealthBar healthBar = null;

	private void Start()
    {
		rigidbody = GetComponent<Rigidbody2D>();
		m_Character = GetComponent<PlatformerCharacter2D>();
		SelectWeapon(0);

		UiManager.Instance.SetAmmo(1, twoAmmo);
		UiManager.Instance.SetAmmo(2, threeAmmo);
	}

	int weaponIndex = 0;
    private void Update()
    {
		if (GameManager.Instance.paused || GameManager.Instance.shouldBeDead)
			return;

		if (pistolShotClock > 0)
		{
			pistolShotClock -= Time.deltaTime;
		}
		if (assaultShotClock > 0)
		{
			assaultShotClock -= Time.deltaTime;
		}
		if (c4ShotClock > 0)
		{
			c4ShotClock -= Time.deltaTime;
		}

		if (!m_Jump)
        {
            // Read the jump input in Update so button presses aren't missed.
            m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }
		if (Input.GetKeyDown(KeyCode.Alpha1))
		{
			SelectWeapon(0);
		}
		if (Input.GetKeyDown(KeyCode.Alpha2))
		{
			SelectWeapon(1);
		}
		if (Input.GetKeyDown(KeyCode.Alpha3))
		{
			SelectWeapon(2);
		}

		CheckClick();
		SetWeaponPositions();
	}

	void SelectWeapon(int index)
	{
		weaponIndex = index;
		if (pistol != null)
			pistol.gameObject.SetActive(index == 0);
		if (assault != null)
			assault.gameObject.SetActive(index == 1);
		if (c4 != null)
			c4.gameObject.SetActive(index == 2);

		UiManager.Instance.SetWeapon(index);
	}
	
	void CheckClick()
	{
		if(Input.GetMouseButton(0))
		{
			int reverser = transform.localScale.x > 0 ? 1 : -1;
			switch (weaponIndex)
			{
				case 0:
					if (pistolShotClock <= 0)
					{
						var createdProjectile = GameObject.Instantiate(pistolProjectile, pistol.position + pistol.right * reverser, pistol.rotation, null);
						createdProjectile.velocity = pistol.right * 14 * reverser;
						createdProjectile.transform.localScale = new Vector3(reverser * createdProjectile.transform.localScale.x, createdProjectile.transform.localScale.y, createdProjectile.transform.localScale.z);
						pistolShotClock = pistolShotTime;
						gunAudio.PlayOneShot(pistolSounds[Random.Range(0, pistolSounds.Count)]);
					}
					break;
				case 1:
					if (assaultShotClock <= 0 && twoAmmo > 0)
					{
						var createdProjectile = GameObject.Instantiate(assaultProjectile, assault.position + assault.right * reverser, assault.rotation, null);
						createdProjectile.velocity = assault.right * 14 * reverser;
						createdProjectile.transform.localScale = new Vector3(reverser * createdProjectile.transform.localScale.x, createdProjectile.transform.localScale.y, createdProjectile.transform.localScale.z);
						assaultShotClock = assaultShotTime;
						gunAudio.PlayOneShot(assaultSounds[Random.Range(0, assaultSounds.Count)]);
						twoAmmo--;
						UiManager.Instance.SetAmmo(1, twoAmmo);
					}
					break;
				case 2:
					if (Input.GetMouseButtonDown(0) && c4ShotClock <= 0 && threeAmmo > 0)
					{
						var createdProjectile = GameObject.Instantiate(c4Projectile, c4.position, c4.rotation, null);
						createdProjectile.velocity = c4.right * 9 * reverser;
						createdProjectile.velocity += rigidbody.velocity * 0.3f;
						createdProjectile.angularVelocity = -400 * reverser;
						createdProjectile.transform.localScale = new Vector3(reverser * createdProjectile.transform.localScale.x, createdProjectile.transform.localScale.y, createdProjectile.transform.localScale.z);
						c4ShotClock = c4ShotTime;
						threeAmmo--;
						UiManager.Instance.SetAmmo(2, threeAmmo);
					}
					break;
			}
		}
	}

	void SetWeaponPositions()
	{
		var mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mouse.z = 0;

		var weapon = Vector3.zero;
		if (pistol != null)
		{
			weapon = pistol.position;
			weapon.z = 0;
			if (transform.localScale.x > 0)
				pistol.right = (mouse - weapon).normalized;
			else
				pistol.right = (weapon - mouse).normalized;
		}

		if (assault != null)
		{
			weapon = assault.position;
			weapon.z = 0;
			if (transform.localScale.x > 0)
				assault.right = (mouse - weapon).normalized;
			else
				assault.right = (weapon - mouse).normalized;
		}

		if (c4 != null)
		{
			weapon = c4.position;
			weapon.z = 0;
			if (transform.localScale.x > 0)
				c4.right = (mouse - weapon).normalized;
			else
				c4.right = (weapon - mouse).normalized;
		}

	}


	private void FixedUpdate()
    {
		if (GameManager.Instance.paused || GameManager.Instance.shouldBeDead)
		{
			GetComponent<Animator>().SetBool("Alive", false);
			return;
		}

		// Read the inputs.
		bool crouch = Input.GetKey(KeyCode.LeftControl);
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        // Pass all parameters to the character control script.
        m_Character.Move(h, crouch, m_Jump);
        m_Jump = false;
    }

	public void SetHealth(int amount)
	{
		health = amount;
		healthBar.SetHealth(health / 100f);


		if (health <= 0 && GameManager.Instance.playerAlive)
		{
			SelectWeapon(-1);
			GameManager.Instance.SetPlayerAlive(false);
		}
	}

	public void AdjustHealth(int amount)
	{
		if (health + amount > 100)
		{
			SetHealth(100);
			var excess = (health + amount) - 100;
			UiManager.Instance.SetInfo("+" + (100 - health).ToString() + " Health");
			GameManager.Instance.AdjustCash(excess * 33);
		}
		else
		{
			UiManager.Instance.SetInfo("+" + amount.ToString() + " Health");
			SetHealth(health + amount);
		}
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		var projectile = collision.gameObject.GetComponent<Projectile>();

		if (projectile != null)
		{
			if (collision.gameObject.GetComponent<PistolProjectile>() != null)
			{
				SetHealth(health - 10);
			}
		}
	}

	public void AddAmmo(int weapon, int ammo)
	{
		if(weapon == 1)
		{
			twoAmmo += ammo;
			UiManager.Instance.SetAmmo(1, twoAmmo);
		}
		else if(weapon == 2)
		{
			twoAmmo += threeAmmo;
			UiManager.Instance.SetAmmo(2, threeAmmo);
		}
	}
}