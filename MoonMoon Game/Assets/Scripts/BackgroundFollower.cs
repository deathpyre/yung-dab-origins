﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundFollower : MonoBehaviour
{
	public Transform player = null;
	public float percentOfPlayer = 0.2f;
	public float bgWidth = 38.4f;

	public float progression = 0;
	Vector3 lastPlayerPos = Vector3.positiveInfinity;
	float halfWidth = 0;
    // Start is called before the first frame update
    void Start()
    {
		halfWidth = bgWidth / 2f;

	}

    // Update is called once per frame
    void Update()
    {
		if (lastPlayerPos.y > 999999)
		{
			lastPlayerPos = player.position;
			return;
		}

		var difference = lastPlayerPos.x - player.position.x;
		difference *= percentOfPlayer;
		progression += difference;

		if(progression < -bgWidth)
		{
			progression += bgWidth;
		}

		if (progression > 0)
		{
			progression -= bgWidth;
		}

		var targetPos = player.position + new Vector3(progression + halfWidth, 0, 0);
		targetPos.y = transform.position.y;
		targetPos.z = transform.position.z;
		transform.position = targetPos;

		lastPlayerPos = player.position;
	}
}
