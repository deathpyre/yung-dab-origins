﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BankColliderReport : MonoBehaviour
{
	[SerializeField] BankController controller = null;

	public GameObject one = null;
	public GameObject two = null;
	public GameObject three = null;

	public string oneDescription = "";
	public string twoDescription = "";

	public AudioClip oneSound = null;
	public AudioClip twoSound = null;
	public AudioClip threeSound = null;

	AudioSource source = null;

	private void Start()
	{
		source = GetComponent<AudioSource>();
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		controller.CollideWith(this);
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		controller.RemoveCollideWith(this);
	}

	public void PlayOne()
	{
		if (oneSound != null)
			source.PlayOneShot(oneSound);
	}
	public void PlayTwo()
	{
		if (twoSound != null)
			source.PlayOneShot(twoSound);
	}
	public void PlayThree()
	{
		if (threeSound != null)
			source.PlayOneShot(threeSound);
	}
}
