﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BankController : Interactable
{
	Platformer2DUserControl controller = null;
	[SerializeField] BankColliderReport frontRoom = null;
	[SerializeField] BankColliderReport backRoom = null;

	[SerializeField] float frontOpenTime = 10;
	[SerializeField] float frontHackTime = 10;

	[SerializeField] float backOpenTime = 1;
	[SerializeField] float backHackTime = 15;

	[SerializeField] SpriteRenderer keycardColorIndicator = null;
	public int chosenKeycard = 0;

	bool collidingFront = false;
	bool collidingBack = false;

	bool hasFrontDoor = true;
	int hasFrontHack = 4;

	bool hasBackDoor = true;
	int hasBackHack = 1;

	BankInteraction currentInteraction = BankInteraction.None;

	// Start is called before the first frame update
	void Start()
    {
		frontRoom.one.SetActive(hasFrontDoor);
		frontRoom.two.SetActive(hasFrontHack != 0);
		frontRoom.three.SetActive(hasFrontHack == 0);

		backRoom.one.SetActive(hasBackDoor);
		backRoom.two.SetActive(hasBackHack != 0);
		backRoom.three.SetActive(hasBackHack == 0);

		chosenKeycard = Random.Range(0, 4);
		switch(chosenKeycard)
		{
			case 0:
				keycardColorIndicator.color = Color.red;
				break;
			case 1:
				keycardColorIndicator.color = Color.blue;
				break;
			case 2:
				keycardColorIndicator.color = Color.green;
				break;
			case 3:
				keycardColorIndicator.color = Color.yellow;
				break;
		}
	}

    // Update is called once per frame
    void Update()
    {
		if (GameManager.Instance.paused || GameManager.Instance.shouldBeDead)
			return;

		if (!Platformer2DUserControl.interacting)
		{
			if (collidingFront)
			{
				if (hasFrontDoor)
				{
					GameManager.Instance.SetInteractionText("Lock Pick");
					if (Input.GetKeyDown(KeyCode.E))
					{
						Platformer2DUserControl.interacting = true;
						GameManager.Instance.SetBadStatus(true);
						currentInteraction = BankInteraction.FD;
						ProgressBar.Instance.SetTimer(frontOpenTime, frontRoom.oneDescription, this);
					}
				}
				else if(hasFrontHack != 0)
				{
					GameManager.Instance.SetInteractionText("Search ("+hasFrontHack.ToString()+"/4)");
					if (Input.GetKeyDown(KeyCode.E))
					{
						Platformer2DUserControl.interacting = true;
						currentInteraction = BankInteraction.FH;
						ProgressBar.Instance.SetTimer(frontHackTime, frontRoom.twoDescription, this);
					}
				}
				else
				{
					GameManager.Instance.SetInteractionText("");
				}
			}
			else if (collidingBack)
			{
				if (hasBackDoor)
				{
					GameManager.Instance.SetInteractionText("Swipe Card");
					if (Input.GetKeyDown(KeyCode.E))
					{
						if (GameManager.Instance.keycardsOwned[chosenKeycard])
						{
							Platformer2DUserControl.interacting = true;
							GameManager.Instance.SetBadStatus(true);
							currentInteraction = BankInteraction.BD;
							ProgressBar.Instance.SetTimer(backOpenTime, backRoom.oneDescription, this);
						}
						else
						{

						}
					}
				}
				else if (hasBackHack != 0)
				{
					GameManager.Instance.SetInteractionText("Open Safe");
					if (Input.GetKeyDown(KeyCode.E))
					{
						Platformer2DUserControl.interacting = true;
						currentInteraction = BankInteraction.BH;
						ProgressBar.Instance.SetTimer(backHackTime, frontRoom.twoDescription, this);
					}
				}
				else
				{
					GameManager.Instance.SetInteractionText("");
				}
			}
		}
    }

	public void CollideWith(BankColliderReport them)
	{
		if(them == frontRoom)
		{
			collidingFront = true;
		}
		else if(them == backRoom)
		{
			collidingBack = true;
		}
	}
	public void RemoveCollideWith(BankColliderReport them)
	{
		if (them == frontRoom)
		{
			collidingFront = false;
			if (currentInteraction == BankInteraction.FD || currentInteraction == BankInteraction.FH)
			{
				ProgressBar.Instance.Break();
				Platformer2DUserControl.interacting = false;
			}
		}
		else if (them == backRoom)
		{
			collidingBack = false;
			if (currentInteraction == BankInteraction.BD || currentInteraction == BankInteraction.BH)
			{
				ProgressBar.Instance.Break();
				Platformer2DUserControl.interacting = false;
			}
		}
		GameManager.Instance.SetInteractionText("");
	}

	int registerVariation = 300;
	public override void CompleteTimer()
	{
		switch (currentInteraction)
		{
			case BankInteraction.None:
				break;
			case BankInteraction.FD:
				hasFrontDoor = false;
				frontRoom.PlayOne();
				break;
			case BankInteraction.FH:
				GameManager.Instance.AdjustCash(1000 + Random.Range(1, registerVariation) - Mathf.RoundToInt(registerVariation/2f));
				hasFrontHack--;
				frontRoom.PlayTwo();
				if (hasFrontHack == 0)
					frontRoom.PlayThree();

				var randomGive = Random.Range(0, 100);
				int emptyRange = 10;
				int assaultRange = 40;
				int c4Range = 30;
				//int keycardRange = 20;
				if (randomGive < 10)
				{

				}
				else if (randomGive < emptyRange + assaultRange)
				{
					var chosen = Random.Range(3, 30);
					GameManager.Instance.GivePlayerAmmo(1, chosen);
					UiManager.Instance.SetInfo("+" + chosen.ToString() + " SMG");
				}
				else if (randomGive < emptyRange + assaultRange + c4Range)
				{
					var chosen = Random.Range(1, 3);
					GameManager.Instance.GivePlayerAmmo(2, chosen);
					UiManager.Instance.SetInfo("+" + chosen.ToString() + " C4");
				}
				else
				{
					GameManager.Instance.GivePlayerKeycard();
				}
				break;
			case BankInteraction.BD:
				hasBackDoor = false;
				backRoom.PlayOne();
				GameManager.Instance.RemoveKeycard(chosenKeycard);
				break;
			case BankInteraction.BH:
				hasBackHack--;
				backRoom.PlayTwo();
				GameManager.Instance.AdjustCash(6000);
				if (hasBackHack == 0)
					backRoom.PlayThree();

				GameManager.Instance.GivePlayerAmmo(2, 3);
				UiManager.Instance.SetInfo("+" + 3 + " C4");
				break;
		}

		frontRoom.one.SetActive(hasFrontDoor);
		frontRoom.two.SetActive(hasFrontHack != 0);
		frontRoom.three.SetActive(hasFrontHack == 0);

		backRoom.one.SetActive(hasBackDoor);
		backRoom.two.SetActive(hasBackHack != 0);
		backRoom.three.SetActive(hasBackHack == 0);

		Platformer2DUserControl.interacting = false;
	}
}

public enum BankInteraction
{
	None,
	FD,
	FH,
	BD,
	BH
}