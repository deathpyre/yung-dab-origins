﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BuildingManager : MonoBehaviour
{
	public Transform cameraObject = null;

	[Header("Bank")]
	public Transform bankPrefab = null;
	List<GameObject> spawnedBuildings = new List<GameObject>();

	[Header("Gas Station")]
	public Transform gasStationPrefab = null;
	List<GameObject> spawnedGasStations = new List<GameObject>();

	[Header("Trash")]
	public Transform trashCanPrefab = null;
	float lastCanSpawnPosition = 0;
	float trashCanSpacing = 40;
	float trashCanVariation = 5;

	[Header("Light")]
	public Transform streetLightPrefab = null;
	float lastLightSpawnPosition = 0;
	float streetLightSpacing = 20;

	[Header("Random")]
	public Transform policeBarrierPrefab = null;
	float lastPoliceBarrierSpawnPosition = 0;
	float policeBarrierSpacing = 40;


	float buildingSpacing = 50;
	float buildingVariation = 15;
	int lastBankDeleted = -1;

	// Start is called before the first frame update
	void Start()
    {
		lastCanSpawnPosition = GameManager.Instance.mapStartPosition;
		lastLightSpawnPosition = GameManager.Instance.mapStartPosition;
		lastPoliceBarrierSpawnPosition = GameManager.Instance.mapStartPosition;
	}


    // Update is called once per frame
    void Update()
    {
		if (GameManager.Instance.paused || GameManager.Instance.shouldBeDead)
			return;

		SpawnBuilding();

		SpawnTrash();

		SpawnStreetLight();

		//SpawnPoliceBarrier();


		if (spawnedBuildings.Count > lastBankDeleted+1 && spawnedBuildings[lastBankDeleted+1].transform.position.x < GameManager.Instance.deletionLinePosition)
		{
			GameObject.Destroy(spawnedBuildings[lastBankDeleted + 1]);
			lastBankDeleted = lastBankDeleted + 1;
		}
	}

	List<int> lastTwoChoices = new List<int>(){ -1, -1 };
	void SpawnBuilding()
	{
		var playerPositionIndex = Mathf.FloorToInt((cameraObject.position.x - GameManager.Instance.mapStartPosition) / buildingSpacing);
		if (playerPositionIndex < 0)
			playerPositionIndex = 0;


		if (spawnedBuildings.Count < playerPositionIndex + 1)
		{
		}
		else
		{
			return;
		}

		var buildingChoice = Random.Range(0, 2);


		if (lastTwoChoices.Where(e => e == 0).Count() >= 2)
			buildingChoice = 1;
		else if (lastTwoChoices.Where(e => e == 1).Count() >= 2)
			buildingChoice = 0;

		lastTwoChoices.RemoveAt(0);
		lastTwoChoices.Add(buildingChoice);

		if (buildingChoice == 0)
		{
			var spawnPosition = GameManager.Instance.mapStartPosition;
			spawnPosition += buildingSpacing * (playerPositionIndex + 1);
			spawnPosition += Random.Range(-buildingVariation, buildingVariation);

			var spawnedBank = GameObject.Instantiate(bankPrefab, new Vector3(spawnPosition, 0, 0), Quaternion.identity, transform).gameObject;
			spawnedBuildings.Add(spawnedBank);
		}
		else if (buildingChoice == 1)
		{
			var spawnPosition = GameManager.Instance.mapStartPosition;
			spawnPosition += buildingSpacing * (playerPositionIndex + 1);
			spawnPosition += Random.Range(-buildingVariation, buildingVariation);

			var spawnedBank = GameObject.Instantiate(gasStationPrefab, new Vector3(spawnPosition, 0, 0), Quaternion.identity, transform).gameObject;
			spawnedBuildings.Add(spawnedBank);
		}
	}

	void SpawnTrash()
	{
		if (GameManager.Instance.furthestX + 50 > lastCanSpawnPosition + trashCanSpacing)
		{
			var spawnX = lastCanSpawnPosition + trashCanSpacing;
			var spawnedTrashCan = GameObject.Instantiate(trashCanPrefab, new Vector3(spawnX, -3.73f, 0), Quaternion.identity, transform).gameObject;

			lastCanSpawnPosition = spawnX;
		}
	}

	void SpawnStreetLight()
	{
		if (GameManager.Instance.furthestX + 50 > lastLightSpawnPosition + streetLightSpacing)
		{
			var spawnX = lastLightSpawnPosition + streetLightSpacing;
			var spawnedStreetLight = GameObject.Instantiate(streetLightPrefab, new Vector3(spawnX, -3.73f, 0), Quaternion.identity, transform).gameObject;

			lastLightSpawnPosition = spawnX;
		}
	}

	float nextRandomPolice = 0;
	void SpawnPoliceBarrier()
	{
		if (GameManager.Instance.furthestX + 50 > lastPoliceBarrierSpawnPosition + policeBarrierSpacing + nextRandomPolice)
		{
			var spawnX = lastPoliceBarrierSpawnPosition + policeBarrierSpacing + nextRandomPolice;
			var spawnedStreetLight = GameObject.Instantiate(policeBarrierPrefab, new Vector3(spawnX, -3.73f, 0), Quaternion.identity, transform).gameObject;

			lastPoliceBarrierSpawnPosition = spawnX;
			nextRandomPolice = Random.Range(-30, 30);
		}
	}
}
