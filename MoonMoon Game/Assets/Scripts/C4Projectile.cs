﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C4Projectile : Projectile
{
	[SerializeField] GameObject explosionRadiusPrefab = null;
	float countdownTime = 2;
	float timeElapsed = 0;
	float explosionPower = 1000;
	float explosionRadius = 3.5f;

	[SerializeField] AudioSource myAudio = null;
	// Start is called before the first frame update
	void Start()
    {
		timeElapsed = 0;
	}

	bool exploded = false;
    // Update is called once per frame
    void Update()
    {
		if (exploded)
			return;
		timeElapsed += Time.deltaTime;

		if(timeElapsed > countdownTime)
		{
			myAudio.PlayOneShot(myAudio.clip);
			var collidingArea = Physics2D.OverlapCircleAll(transform.position, explosionRadius);
			for(int i = 0; i < collidingArea.Length; i++)
			{
				if(stuckWall == null)
				{
					if (collidingArea[i].gameObject.layer == LayerMask.NameToLayer("BankWall"))
					{
						stuckWall = collidingArea[i].gameObject;
					}
				}
				var foundRigidbody = collidingArea[i].GetComponent<Rigidbody2D>();
				if (foundRigidbody != null && collidingArea[i].GetType() != typeof(CircleCollider2D))
				{
					var difference = collidingArea[i].ClosestPoint(transform.position) - new Vector2(transform.position.x, transform.position.y);
					var inverseDistance = explosionRadius - difference.magnitude;
					foundRigidbody.AddForce(difference.normalized * explosionPower);// * (inverseDistance / (float)explosionRadius));
					var playerScript = foundRigidbody.GetComponent<Platformer2DUserControl>();
					if (playerScript != null)
						playerScript.AdjustHealth(-25);

					var botScript = foundRigidbody.GetComponent<TerminatorBot>();
					if (botScript != null)
						botScript.AdjustHealth(-100);
				}
			}

			GameManager.Instance.SetBadStatus(true);
			var explosionVisual = GameObject.Instantiate(explosionRadiusPrefab, transform.position, Quaternion.identity);
			iTween.ScaleFrom(explosionVisual, Vector3.zero, 0.5f);
			GameObject.Destroy(explosionVisual, 0.5f);
			if (stuckWall != null)
			{
				stuckWall.GetComponentInChildren<SpriteRenderer>().enabled = false;
				stuckWall.GetComponent<BoxCollider2D>().enabled = false;
				Destroy(stuckWall,1);
			}
			Destroy(gameObject,1);
			GetComponent<SpriteRenderer>().enabled = false;
			exploded = true;
		}
	}

	GameObject stuckWall = null;
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.layer == LayerMask.NameToLayer("BankWall"))
		{
			stuckWall = collision.gameObject;
		}
		transform.SetParent(collision.gameObject.transform);
		GetComponent<Rigidbody2D>().simulated = false;
	}
}
