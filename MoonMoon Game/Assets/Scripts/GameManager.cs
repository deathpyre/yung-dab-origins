﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
	public static GameManager Instance = null;
	int cash = 0;

	[SerializeField] AudioSource introMusic = null;
	[SerializeField] AudioSource fullMusic = null;
	[Header("UI")]
	[SerializeField] GameObject pauseMenu = null;
	[SerializeField] Text cashText = null;
	[SerializeField] Text cashChangeText = null;
	[SerializeField] Text interactionText = null;
	[SerializeField] Image fiveStar = null;

	[SerializeField] GameObject bustedUi = null;
	[SerializeField] Image bustedSlider = null;

	[Header("POLICE")]
	public float mapStartPosition = -5991;
	[SerializeField] Transform policeVisual = null;
	[SerializeField] Transform policeSiren = null;
	[SerializeField] Transform invisibleWall = null;

	[SerializeField] Transform camera = null;

	public Platformer2DUserControl player = null;
	public bool playerAlive = true;

	public float furthestX = 0;
	public float deletionLinePosition = 0;

	public bool isABadBoy = false;
	public bool shouldBeDead = false;

	// Start is called before the first frame update
	void Awake()
    {
		Instance = this;
		SetGameSpeed(1);
		furthestX = mapStartPosition;
		UpdatePlayerDeletionLine();
		SetCash(0);
		player.SetHealth(100);
		fiveStar.color = Color.clear;
		//AudioListener.volume = 1;
		AudioListener.volume = PlayerPrefs.GetFloat("MasterVolume", 1);
	}

	private void Start()
	{
	}
	
	float deadTimer = 0;
    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			SetGameSpeed(paused ? 1 : 0);
		}

		if (shouldBeDead)
		{
			deadTimer += Time.deltaTime;
			if (deadTimer > 5)
			{
				SetGameSpeed(0);
			}
			return;
		}

		CheckPlayerInPolice();

		UpdatePlayerDeletionLine();

		if (camera.position.x < policeVisual.position.x)
		{
			policeSiren.position = camera.position;
		}
		else
		{
			var targetPos = camera.position;
			targetPos.x = policeVisual.position.x;
			policeSiren.position = targetPos;
		}
	}

	int counter = 0;
	private void LateUpdate()
	{
		if (counter < 2)
		{
			counter++;
			fullMusic.Pause();
		}
	}

	public void SetBadStatus(bool status)
	{
		if (!isABadBoy)
		{
			iTween.ValueTo(gameObject, iTween.Hash(
				 "from", status ? 0 : 1,
				 "to", status ? 1 : 0,
				 "oncompletetarget", gameObject,
				 "oncomplete", "FadeUpFinished",
				 "time", 3,
				 "easetype", iTween.EaseType.easeOutBounce,
				 "onupdate", (Action<object>)(newVal =>
				 {
					 fiveStar.color = new Color(1, 1, 1, (float)newVal);
				 }
			 )));
		}
		else
		{
			fiveStar.color = Color.clear;
		}

		isABadBoy = status;
		if (isABadBoy)
		{
			introMusic.Pause();
			fullMusic.UnPause();
		}
		else
		{
			introMusic.UnPause();
			fullMusic.Pause();
		}
	}

	private void UpdatePlayerDeletionLine()
	{
		if (player.transform.position.x > furthestX)
		{
			furthestX = player.transform.position.x;
			deletionLinePosition = furthestX - 60;
			if (deletionLinePosition < mapStartPosition)
				deletionLinePosition = mapStartPosition;
			invisibleWall.position = new Vector3(deletionLinePosition, invisibleWall.position.y, invisibleWall.position.z);
		}
	}

	float bustedTimer = 0;
	float bustedLength = 12;
	void CheckPlayerInPolice()
	{
		if (player.transform.position.x < policeVisual.position.x)
		{
			if (!bustedUi.activeSelf)
				bustedUi.SetActive(true);

			bustedTimer += Time.deltaTime;
			bustedSlider.fillAmount = bustedTimer / bustedLength;
			if(bustedTimer >= bustedLength)
			{
				shouldBeDead = true;
				playerAlive = false;
				deadTimer = 0;
			}
		}
		else
		{
			if (bustedUi.activeSelf)
			{
				bustedTimer = 0;
				bustedUi.SetActive(false);
			}
		}
	}

	public void SetCash(int cashSet)
	{
		cash = cashSet;
		FormatCash();
	}

	int lastAdjust = 0;
	public void AdjustCash(int adjustment)
	{
		cash += adjustment;
		
		cashChangeText.color = new Color(1, 1, 1, 0);

		iTween.ValueTo(gameObject, iTween.Hash(
			 "from", 0,
			 "to", 1,
			 "oncompletetarget", gameObject,
			 "oncomplete", "FadeUpFinished",
			 "easetype", iTween.EaseType.easeOutExpo,
			 "onupdate", (Action<object>)(newVal => {
				 cashChangeText.color = new Color(1, 1, 1, (float)newVal);
				 //Debug.LogError(Mathf.RoundToInt(255 * (float)newVal).ToString("x"));
				 lastAdjust = adjustment;
				 var hexVal = Mathf.RoundToInt(255 * (float)newVal).ToString("x");
				 if (hexVal == "0")
					 hexVal += "0";
				 cashChangeText.text = "+<color=#118e12" + hexVal + ">$</color> " + (lastAdjust == 0 ? "0" : lastAdjust.ToString("#,#"));
			 }
		 )));
		//iTween.FadeTo(cashChangeText.gameObject, iTween.Hash("alpha",1,"time", 3, "looptype",iTween.LoopType.pingPong));
		FormatCash();
	}

	void FadeUpFinished()
	{
		iTween.ValueTo(gameObject, iTween.Hash(
			 "from", 1,
			 "to", 0,
			 "easetype", iTween.EaseType.easeInExpo,
			 "onupdate", (Action<object>)(newVal => {
				 cashChangeText.color = new Color(1, 1, 1, (float)newVal);
				 var hexVal = Mathf.RoundToInt(255 * (float)newVal).ToString("x");
				 if (hexVal == "0")
					 hexVal += "0";
				 cashChangeText.text = "+<color=#118e12" + hexVal + ">$</color> " + (lastAdjust == 0 ? "0" : lastAdjust.ToString("#,#"));
			 }
		 )));
	}

	void FormatCash()
	{
		cashText.text = "<color=#118e12ff>$</color> " + (cash == 0 ? "0" : cash.ToString("#,#"));
	}

	public bool paused = false;
	public void SetGameSpeed(float speed)
	{
		Time.timeScale = speed;

		paused = speed == 0;		
		if (paused)
		{
			//AudioListener.volume = 0f;
			introMusic.UnPause();
			fullMusic.Pause();
			policeSiren.GetComponent<AudioSource>().Pause();
		}
		else
		{
			//AudioListener.volume = PlayerPrefs.GetFloat("MasterVolume", 1);
			if(isABadBoy)
			{
				introMusic.Pause();
				fullMusic.UnPause();
			}
			else
			{
				introMusic.UnPause();
				fullMusic.Pause();
			}
			policeSiren.GetComponent<AudioSource>().Play();
		}

		pauseMenu.SetActive(paused);
		
	}

	public void SetPlayerAlive(bool alive)
	{
		playerAlive = alive;
		if (!shouldBeDead && !playerAlive)
		{
			shouldBeDead = true;
			deadTimer = 0;
		}
	}

	public void RestartGame()
	{
		SceneManager.LoadScene("Main Scene");
	}

	public void SetInteractionText(string text)
	{
		if (interactionText.text == text)
			return;

		if (text == "")
			interactionText.text = text;
		else
			interactionText.text = "Press 'E' to "+text;
	}

	public void SetMusicVolume(float value)
	{
		introMusic.volume = value;
		fullMusic.volume = value;
	}

	public void GivePlayerAmmo(int weapon, int ammo)
	{
		player.AddAmmo(weapon, ammo);
	}

	public List<bool> keycardsOwned = new List<bool>() { false, false, false, false };
	public void GivePlayerKeycard()
	{
		var availible = keycardsOwned.FindAll(e => e == false);
		var chosen = Random.Range(0, availible.Count);

		int counted = 0;
		for(int i = 0; i < keycardsOwned.Count; i++)
		{
			if(!keycardsOwned[i])
			{
				counted++;
				if(counted > chosen)
				{
					keycardsOwned[i] = true;
					UiManager.Instance.SetCard(i, true);

					var cardName = "Red";
					switch(i)
					{
						case 1:
							cardName = "Blue";
							break;
						case 2:
							cardName = "Yellow";
							break;
						case 3:
							cardName = "Green";
							break;
					}
					UiManager.Instance.SetInfo("+" + cardName + " Keycard");
					break;
				}
			}
		}
	}

	public void RemoveKeycard(int index)
	{
		keycardsOwned[index] = false;
		UiManager.Instance.SetCard(index, false);
	}
}
