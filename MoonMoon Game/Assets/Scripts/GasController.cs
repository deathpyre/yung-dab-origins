﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasController : Interactable
{
	Platformer2DUserControl controller = null;
	[SerializeField] GasColliderReport medsArea = null;

	[SerializeField] float medsOpenTime = 10;

	bool collidingMeds = false;
	
	int hasMeds = 3;

	GasInteration currentInteraction = GasInteration.None;

	// Start is called before the first frame update
	void Start()
    {
		medsArea.one.SetActive(hasMeds != 0);
	}

	// Update is called once per frame
	void Update()
    {
		if (!Platformer2DUserControl.interacting)
		{
			if (collidingMeds)
			{
				if(hasMeds != 0)
				{
					GameManager.Instance.SetInteractionText("Steal Bandage ("+ hasMeds.ToString()+"/3)");
					if (Input.GetKeyDown(KeyCode.E))
					{
						GameManager.Instance.SetBadStatus(true);
						Platformer2DUserControl.interacting = true;
						currentInteraction = GasInteration.Meds;
						ProgressBar.Instance.SetTimer(medsOpenTime, medsArea.oneDescription, this);
					}
				}
				else
				{
					GameManager.Instance.SetInteractionText("");
				}
			}
		}
    }

	public void CollideWith(GasColliderReport them)
	{
		if(them == medsArea)
		{
			collidingMeds = true;
		}
	}
	public void RemoveCollideWith(GasColliderReport them)
	{
		if (them == medsArea)
		{
			collidingMeds = false;
			if (currentInteraction == GasInteration.Meds)
			{
				ProgressBar.Instance.Break();
				Platformer2DUserControl.interacting = false;
			}
		}
		GameManager.Instance.SetInteractionText("");
	}

	int registerVariation = 300;
	public override void CompleteTimer()
	{
		switch (currentInteraction)
		{
			case GasInteration.None:
				break;
			case GasInteration.Meds:
				GameManager.Instance.player.AdjustHealth(15);
				hasMeds--;
				medsArea.PlayOne();
				if (hasMeds == 0)
					medsArea.PlayTwo();
				break;
		}

		medsArea.one.SetActive(hasMeds != 0);

		Platformer2DUserControl.interacting = false;
	}
}

public enum GasInteration
{
	None,
	Meds
}