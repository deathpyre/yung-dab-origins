﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseScreenManager : MonoBehaviour
{
	public static PauseScreenManager Instance = null;

	[SerializeField] GameObject mainPause = null;
	[SerializeField] GameObject optionsPause = null;

	[SerializeField] Button restartButton = null;
	[SerializeField] Button resumeButton = null;
	[SerializeField] Button optionsButton = null;
	[SerializeField] Button quitButton = null;
	[SerializeField] Slider volumeSlider = null;
	[SerializeField] Slider musicVolumeSlider = null;

	void Awake()
	{
		Instance = this;
	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnEnable()
	{
		//restartButton.gameObject.SetActive(!GameManager.Instance.playerAlive);
		resumeButton.interactable = GameManager.Instance.playerAlive;
		volumeSlider.value = PlayerPrefs.GetFloat("MasterVolume", 1);
		musicVolumeSlider.value = PlayerPrefs.GetFloat("MusicVolume", 1);

		mainPause.SetActive(true);
		optionsPause.SetActive(false);
	}

	public void OptionsPressed()
	{
		mainPause.SetActive(false);
		optionsPause.SetActive(true);
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void SetVolume(float value)
	{
		AudioListener.volume = value;
		PlayerPrefs.SetFloat("MasterVolume", value);
	}

	public void SetMusicVolume(float value)
	{
		GameManager.Instance.SetMusicVolume(value);
		PlayerPrefs.SetFloat("MusicVolume", value);
	}

	public void BackToMain()
	{
		mainPause.SetActive(true);
		optionsPause.SetActive(false);
	}

	public void BackToStart()
	{
		SceneManager.LoadScene(0);
	}
}
