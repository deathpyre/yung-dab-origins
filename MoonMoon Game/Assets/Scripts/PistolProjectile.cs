﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PistolProjectile : Projectile
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

	float timeTravelled = 0;
	// Update is called once per frame
	void Update()
	{
		timeTravelled += Time.deltaTime;
		if(timeTravelled > 10)
			GameObject.Destroy(gameObject);
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		//if (collision.gameObject.layer == LayerMask.NameToLayer("BankWall"))
		{
		}
		GameObject.Destroy(gameObject);
	}
}
