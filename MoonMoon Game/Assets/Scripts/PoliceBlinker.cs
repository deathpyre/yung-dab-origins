﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PoliceBlinker : MonoBehaviour
{
	[SerializeField] float policeMovementRate = 1;
	[SerializeField] float flashRate = 1.5f;

	[SerializeField] SpriteRenderer blinkingBox = null;
	[SerializeField] AudioSource sirenSound = null;
	
	float timeTaken = 0;

	[SerializeField] HealthBar healthBar = null;
	int health = 100;

	bool colorRed = false;
    // Start is called before the first frame update
    void Start()
    {
		blinkingBox.gameObject.SetActive(false);
		sirenSound.mute = true;
		healthBar.SetHealth(1);
	}

    // Update is called once per frame
    void Update()
    {
		if(!GameManager.Instance.isABadBoy)
		{
			return;
		}

		if (!blinkingBox.gameObject.activeSelf)
		{
			sirenSound.mute = false;
			blinkingBox.gameObject.SetActive(true);
		}

		//TODO make police always be INFRONT of deletion line
		transform.position += new Vector3(policeMovementRate * Time.deltaTime, 0, 0);

		if (transform.position.x < GameManager.Instance.deletionLinePosition)
			transform.position = new Vector3(GameManager.Instance.deletionLinePosition, transform.position.y, transform.position.z);

		timeTaken += Time.deltaTime;
		if (timeTaken >= flashRate)
		{
			timeTaken -= flashRate;
			colorRed = !colorRed;

			blinkingBox.color = colorRed ? new Color(1,0,0,0.2f) : new Color(0, 0, 1, 0.2f);
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		var projectile = collision.gameObject.GetComponent<Projectile>();

		if (projectile != null && !projectile.startedInSiren)
		{
			if (collision.gameObject.GetComponent<PistolProjectile>() != null)
			{
				GameObject.Destroy(collision.gameObject);
				transform.position -= new Vector3(0.3f, 0, 0);
				health -= 5;

				if (health <= 0)
				{
					health += 100;
					transform.position -= new Vector3(10f, 0, 0);
				}
				healthBar.SetHealth(health / 100f);
			}

			projectile.startedInSiren = true;
		}
	}
}
