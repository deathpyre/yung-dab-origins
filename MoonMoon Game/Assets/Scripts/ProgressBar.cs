﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
	public static ProgressBar Instance = null;

	[SerializeField] Image fillImage = null;
    [SerializeField] Text barText = null;

	float timeToTake = 0;
	float timeTaken = 0;

	Interactable toTrigger = null;
    // Start is called before the first frame update
    void Awake()
    {
		Instance = this;
		gameObject.SetActive(false);
	}

    // Update is called once per frame
    void Update()
    {
        if(timeToTake > 0)
		{
			timeTaken += Time.deltaTime;
			fillImage.fillAmount = Mathf.Clamp(timeTaken / timeToTake, 0, 1);
			if(timeTaken >= timeToTake)
			{
				timeToTake = 0;
				toTrigger.CompleteTimer();
				gameObject.SetActive(false);
			}
		}
    }

	public void SetTimer(float length, string title, Interactable them)
	{
		barText.text = title;
		timeToTake = length;
		timeTaken = 0;
		gameObject.SetActive(true);
		toTrigger = them;
	}

	public void Break()
	{
		timeToTake = 0;
		gameObject.SetActive(false);
	}
}
