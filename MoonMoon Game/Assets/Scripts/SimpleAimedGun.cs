﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAimedGun : MonoBehaviour
{
	Transform player = null;
	[SerializeField] Rigidbody2D bulletPrefab = null;
	[SerializeField] bool canShoot = true;

	// Start is called before the first frame update
	void Start()
    {
		player = GameObject.FindGameObjectWithTag("Player").transform;
	}

    // Update is called once per frame
    void Update()
    {
		if (!GameManager.Instance.isABadBoy)
		{
			return;
		}

		if (Vector3.Distance(transform.position, player.position) < 24)
		{
			ShootAtPlayer();
		}
    }

	[Header("Pistol")]
	[SerializeField] List<AudioClip> pistolSounds = new List<AudioClip>();
	[SerializeField] AudioSource gunAudio = null;
	float pistolShotTimeMin = 0.20f;
	float pistolShotTimeMax = 1f;
	float pistolShotClock = 0;
	void ShootAtPlayer()
	{
		var weapon = transform.position;
		weapon.z = 0;
		if (transform.localScale.x > 0)
			transform.right = (GameManager.Instance.player.transform.position - weapon).normalized;
		else
			transform.right = (weapon - GameManager.Instance.player.transform.position).normalized;

		if (!canShoot)
			return;

		if (pistolShotClock > 0)
		{
			pistolShotClock -= Time.deltaTime;
		}
		
		if (pistolShotClock <= 0)
		{
			transform.rotation *= Quaternion.Euler(new Vector3(0, 0, Random.Range(-5f, 14f)));
			var createdProjectile = GameObject.Instantiate(bulletPrefab, transform.position+transform.right, transform.rotation, null);
			createdProjectile.velocity = transform.right * 14;
			pistolShotClock = Random.Range(pistolShotTimeMin, pistolShotTimeMax);
			gunAudio.PlayOneShot(pistolSounds[Random.Range(0, pistolSounds.Count)]);
		}
	}
}
