﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerminatorBot : MonoBehaviour
{
	[SerializeField] Animator animator = null;
	[SerializeField] float speed = 0.2f;

	public bool canShoot = false;
	[SerializeField] HealthBar healthBar = null;
	[SerializeField] GameObject gun = null;

	[SerializeField] GameObject hat = null;

	float health = 100;
	// Start is called before the first frame update
	void Start()
    {
		animator.SetBool("Ground", true);
		animator.SetFloat("Speed", speed);
		if (canShoot)
			SetHealth(100);
	}

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (canShoot)
		{
			var projectile = collision.gameObject.GetComponent<Projectile>();

			if (projectile != null)
			{
				if (collision.gameObject.GetComponent<PistolProjectile>() != null)
				{
					SetHealth(health - 15);
				}
			}
		}
	}

	public void AdjustHealth(float amount)
	{
		if (canShoot)
			SetHealth(health + amount);
	}

	void SetHealth(float newHealth)
	{
		health = newHealth;
		healthBar.SetHealth(health / 100f);

		if(health <= 0)
		{
			animator.SetBool("Alive", false);
			gameObject.layer = LayerMask.NameToLayer("DeadPeople");
			hat.SetActive(false);
			gun.SetActive(false);
			//GetComponent<Rigidbody2D>().simulated = false;
			//GetComponent<BoxCollider2D>().enabled = false;
			//GetComponent<CircleCollider2D>().enabled = false;
		}
	}
}
