﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UiManager : MonoBehaviour
{
	public static UiManager Instance = null;
	public List<Transform> banners = new List<Transform>();
	public List<Image> keyCards = new List<Image> ();

	public Text info = null;
	// Start is called before the first frame update
	void Awake()
    {
		Instance = this;
		foreach (var card in keyCards)
			card.color = Color.clear;

		info.text = "";
	}

    // Update is called once per frame
    void Update()
    {
    }

	public void SetAmmo(int weapon, int ammo)
	{
		var text = banners[weapon].GetComponentInChildren<Text>();
		if (ammo == 0)
			text.color = Color.red;
		else
			text.color = Color.white;

		text.text = ammo.ToString();
	}

	public void SetWeapon(int index)
	{
		for(int i = 0; i < banners.Count; i++)
		{
			banners[i].localPosition = new Vector3(banners[i].localPosition.x, index == i ? -45: 0, banners[i].localPosition.z);
		}
	}

	public void SetCard(int index, bool on)
	{
		keyCards[index].color = on ? Color.white : Color.clear;
	}

	public void SetInfo(string text)
	{
		info.text = text;

		iTween.ValueTo(gameObject, iTween.Hash(
				 "from", 1,
				 "to", 0,
				 "oncompletetarget", gameObject,
				 "oncomplete", "FadeUpFinished",
				 "time", 5,
				 "easetype", iTween.EaseType.linear,
				 "onupdate", (Action<object>)(newVal =>
				 {
					 info.color = new Color(1, 1, 1, (float)newVal);
				 }
			 )));
	}
}
